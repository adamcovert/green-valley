$( document ).ready(function() {

  $('.burger').on('click', function () {
    if ( !$('.page-header__nav-menu').hasClass('page-header__nav-menu--is-active') ) {
      $('.page-header__nav-menu').addClass('page-header__nav-menu--is-active');
    } else {
      $('.page-header__nav-menu').removeClass('page-header__nav-menu--is-active');
    }
  });
});