$(document).ready(function() {

  $('.rest-types__nav ul li a').on('click',function(event){
    event.preventDefault();
    var ident = $(this).attr('href');

    $('.rest-types__nav ul li a').removeClass('is-active');
    $(this).closest('.rest-types__nav ul li a').addClass('is-active');

    $('.rest-types__content').removeClass('rest-types__content--is-active');
    $(this).closest('.rest-types').find(ident).addClass('rest-types__content--is-active');
  });
});